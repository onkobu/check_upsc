# check_upsc

Icinga/ Nagios check command for NUT/ UPSC uninterruptible power supply.

```
Check UPSC

check_upsc [-h]
check_upsc -V
check_upsc [-t <test-file>] 
check_upsc [-d <device>] [OPTIONS]

-h             - print usage and exit
-V             - print version and exit
-t <test-file> - read properties from given file
-d <device>    - device that will be queried by command

Options:

-v             - verbose output
-b <percent>   - Warning threshold, value battery.charge
-r <seconds>   - Warning threshold, value battery.runtime
-l <locale>    - Locale, for example C to use . as float separator

It is not possible to override critical threshold as they
are determined by the UPS/ its driver. These thresholds force
a shutdown and masking them with monitoring values is
dangerous.
```

# Preconditions

1. Get a uninterruptiple power supply that is supported by Linux/ NUT/ ``upsc`` (see links below)
1. Setup [Network UPS Tools](https://networkupstools.org/)
1. Verify ''upsc'' prints the correct values (have a look at the folder eaton-samples)
1. Copy this script to a local directory, e.g. ``/usr/local/lib64/icinga2/plugins/`` (local plugin directory)
1. Create a *check command* in your Icinga2 configuration
1. Assign this check command with a Icinga2 *service definition* to the responsible host
1. Enjoy UPS-aware Icinga 2


```
# /etc/icinga2/conf.d/constants
#
const PluginLocalDir="/usr/local/lib64/icinga2/plugins"

# /etc/icinga2/conf.d/commands.conf
#
object CheckCommand "check-upsc-local" {
	command = [ PluginLocalDir + "/check_upsc.sh" ]
	arguments = {
		"-d" = {
			value = "YOURDevice@localhost"
			description = "The device supplying power in case of line failure"
			required = true
		}
		"-b" = {
			value = "$battery_charge_warning$"
			description = "Battery charge in percent that does not trigger WARNING yet"
			required = false
		}
		"-r" = {
			value = "$battery_runtime_warning$"
			description = "Remaining runtime in seconds that does not trigger WARNING yet"
			required = false
		}
	}
}

# /etc/icinga2/conf.d/services.conf

apply Service "upsc" {
	import "generic-service"
	# see /usr/share/icinga2/include/plugins-contrib.d/hardware.conf
	# and local adaption in commands.conf
	check_command = "check-upsc-local"
	
    # default values shown
	#vars.battery_charge_warning = "50"
	#vars.battery_runtime_warning = "300"

	assign where host.name == NodeName
}

```

Bonus:

Setup [PNP4Nagios](https://docs.pnp4nagios.org/) to graph the output, too.

# Testing

With parameter ``-t`` a file can be injected instead of reading command output. Use this if your device yields an output that does not pass the check script successfully. Compare with samples in ``eaton-samples`` for the details.

```
# test run with an output that was recording with
# low battery and pending forced shutdown
#
check_upsc.sh -t eaton-samples/status_bat_fsd
```

# Hints

* Standard setup of NUT renders log messages with substring *UPS*
* upsmon or upsdrv sometimes die or loose connection, output then becomes *Stale Data*
* Depending on the UPS critical thresholds cause system shutdown automatically thus no critical thresholds can be set through the plugin

# Links

* NUT's [compatibility list](https://networkupstools.org/stable-hcl.html)
